<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FleetLine extends Model
{

  public function shipType() {
    return $this->belongsTo('App\ShipType');
  }

  public function fleet() {
    return $this->belongsTo('App\Fleet');
  }

}
