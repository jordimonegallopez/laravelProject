<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fleet extends Model
{

  public function fleetLines() {
    return $this->hasMany('App\FleetLine');
  }

  public function user() {
    return $this->belongsTo('App\User');
  }

}
