<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShipType;

class NausController extends Controller
{
    public function naus() {
      $shipTypes = ShipType::all();

      return view('naus')->with('shipTypes', $shipTypes);
    }
}
