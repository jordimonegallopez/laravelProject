<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Planet;

class PlanetController extends Controller
{

  public static function agafarPlanetaLliure(){
    $planeta = Planet::doesntHave('user')->first();
    return $planeta;
  }

  public function resum() {
    return view('resum');
  }

  public function hola() {
    return view('home');
  }

  public function atacar() {
    $planets = Planet::all();
    return view('atacar')->with('planets', $planets);
  }





}
