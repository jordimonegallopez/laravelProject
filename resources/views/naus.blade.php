@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Pantalla Naus</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-md-12">
                      <p>Escull un tipus de nau per crear<p>
                      <select name="tipusNaus">
                        @foreach ($shipTypes as $shipType)
                              <option value="selectShipType"> {{ $shipType->name  }} </option> <br>
                        @endforeach
                      </select>
                      <br><br>
                      <p>Introdueix el numero de naus que vols  crear: </p>
                      <input type="integer" name="numShips">
                      <br><br>
                      <button type="button" class="btn btn-primary active">Crear</button12>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
      <div class="col-md-12 styleButton">
        <div class ="btn-group">
          <a href="{{ action('PlanetController@resum') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Resum </a>

      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 styleButton">
      <div class ="btn-group">
        <a href="{{ action('PlanetController@atacar') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Atacar </a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('NausController@naus') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Naus </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Investigacions </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Recursos </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Defenses </a>

  </div>
</div>
</div>

<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Hangar </a>

  </div>
</div>
</div>

<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"   role="button" aria-disabled="true"> Instalacions </a>

  </div>
</div>
</div>
</div>



@endsection
