@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Pantalla Atacar</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="col-md-12">

                    <form action="{{ action('AtacarController@atacar') }}">

                    <select name="Planeta">
                      @foreach ($planets as $planet)
                             <option value="{{ $planet->name  }}"> {{ $planet->name  }} </option> <br>
                      @endforeach
                    </select>

                    <input type="submit" value="Atacar"/>

                    </form>

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
      <div class="col-md-12 styleButton">
        <div class ="btn-group">
          <a href="{{ action('PlanetController@resum') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Resum </a>

      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 styleButton">
      <div class ="btn-group">
        <a href="{{ action('PlanetController@atacar') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Atacar </a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('NausController@naus') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Naus </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Investigacions </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Recursos </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Defenses </a>

  </div>
</div>
</div>

<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Hangar </a>

  </div>
</div>
</div>

<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"   role="button" aria-disabled="true"> Instalacions </a>

  </div>
</div>
</div>






</div>

</div>

@endsection
