@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

              <div class="panel-heading">Resum Jugador</div>


                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif


              <h1>Usuari:  {{Auth::user()->name}}</h1>
              <br>
              <h1>Planeta</h1>

              <table style="width:100%">
               <tr>
                 <th>Nom planeta</th>
                 <td>{{Auth::user()->planets[0]->name}}</td>
               </tr>
               <tr>
                 <th>Tipus</th>
                 <td>{{Auth::user()->planets[0]->type}}</td>
               </tr>
               <tr>
                 <th>Sistema solar</td>
                 <td>{{Auth::user()->planets[0]->solarSistem}}</td>
               </tr>
               <tr>
                 <th>Posicio</td>
                 <td>{{Auth::user()->planets[0]->position}}</td>
               </tr>
              </table>

            </div>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
      <div class="col-md-12 styleButton">
        <div class ="btn-group">
          <a href="{{ action('PlanetController@resum') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Resum </a>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 styleButton">
      <div class ="btn-group">
        <a href="{{ action('PlanetController@atacar') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Atacar </a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('NausController@naus') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Naus </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Investigacions </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class ="btn-group styleButton">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Recursos </a>
  </div>
</div>
</div>
<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida" role="button" aria-disabled="true"> Defenses </a>

  </div>
</div>
</div>

<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"  role="button" aria-disabled="true"> Hangar </a>

  </div>
</div>
</div>

<div class="row">
  <div class="col-md-12 styleButton">
    <div class ="btn-group">
      <a href="{{ action('PlanetController@hola') }}" class="btn btn-primary btn-md btn-block mateixamida"   role="button" aria-disabled="true"> Instalacions </a>

  </div>
</div>
</div>
</div>



@endsection
