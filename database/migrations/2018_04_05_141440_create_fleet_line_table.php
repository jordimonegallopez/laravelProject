<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFleetLineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fleet_lines', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('fleet_id')->unsigned()->nullable()->default(null);
            $table->foreign('fleet_id')->references('id')->on('fleets')->onDelete('cascade');

            $table->integer('ship_id')->unsigned()->nullable()->default(null);
            $table->foreign('ship_id')->references('id')->on('ship_types')->onDelete('cascade');



            $table->integer('quantity')->unsigned()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fleet_lines');
    }
}
