<?php

use Illuminate\Database\Seeder;

class ShipTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('ship_types')->insert([
          'name' => 'Cazador ligero',
          'speed' =>20000,
          'weight' =>30,
          'attack' =>55,
          'hp' =>15,
          'capacity' =>50,
        ]);
        DB::table('ship_types')->insert([
          'name' => 'Crucero',
          'speed' =>30000,
          'weight' =>30,
          'attack' =>440,
          'hp' =>75,
          'capacity' =>800,
        ]);
        DB::table('ship_types')->insert([
          'name' => 'Nave grande de carga',
          'speed' =>12000,
          'weight' =>30,
          'attack' =>5.5,
          'hp' =>37.5,
          'capacity' =>25000,
        ]);
    }
}
