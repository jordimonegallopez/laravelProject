<?php

use Illuminate\Database\Seeder;

class PlanetesSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){

      DB::table('planets')->insert([
        'name' => 'Jupiter',
        'type' => 'Aqua',
        'position' => 1,
        'created_at' => date('Y-m-d H:i:s'),
      ]);
      DB::table('planets')->insert([
        'name' => 'Saturno',
        'type' => 'Fire',
        'position' => 2,
        'created_at' => date('Y-m-d H:i:s'),
      ]);
      DB::table('planets')->insert([
        'name' => 'Venus',
        'type' => 'Desert',
        'position' => 3,
        'created_at' => date('Y-m-d H:i:s'),
      ]);
      DB::table('planets')->insert([
        'name' => 'Marte',
        'type' => 'Fire',
        'position' => 4,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Terra',
        'type' => 'Fire',
        'position' => 5,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Joker',
        'type' => 'Fire',
        'position' => 6,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta1',
        'type' => 'Fire',
        'position' => 7,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta2',
        'type' => 'Fire',
        'position' => 8,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta3',
        'type' => 'Fire',
        'position' => 9,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta4',
        'type' => 'Fire',
        'position' => 10,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta5',
        'type' => 'Fire',
        'position' => 11,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta6',
        'type' => 'Fire',
        'position' => 12,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta7',
        'type' => 'Fire',
        'position' => 13,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta8',
        'type' => 'Fire',
        'position' => 14,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta9',
        'type' => 'Fire',
        'position' => 15,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta10',
        'type' => 'Fire',
        'position' => 16,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta11',
        'type' => 'Fire',
        'position' => 17,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta12',
        'type' => 'Fire',
        'position' => 18,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta13',
        'type' => 'Fire',
        'position' => 19,
        'created_at' => date('Y-m-d H:i:s'),

      ]);
      DB::table('planets')->insert([
        'name' => 'Planeta14',
        'type' => 'Fire',
        'position' => 20,
        'created_at' => date('Y-m-d H:i:s'),

      ]);

        //
    }
}
